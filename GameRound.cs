public class GameRound
{
    private Player player1;
    private Player player2;
    private Player? _roundWinner;

    public Player? RoundWinner
   {
       get => _roundWinner;
   }

    public GameRound(Player player1, Player player2)
    {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void PlayRound()
    {
        Gesture playerOneGesture = player1.PlayGesture();
        Gesture playerTwoGesture = player2.PlayGesture();
        if (playerOneGesture.Equals(playerTwoGesture))
        {
            return;
        }
        if(player1.PlayGesture().Beats(player2.PlayGesture()))
        {
            _roundWinner = player1;  
            return;    
        }
        else
        {
            _roundWinner = player2;  
            return;         
        }
     
    }
}

