public class ComputerPlayer : Player
{   
    public ComputerPlayer(string name)
    : base(name)
    {
        
    }

    public override Gesture PlayGesture()
    {
        return _gesture;
    }

    public void ChooseNextGesture()
    {          
        if (_gesture == null)
        {
            _gesture = new Gesture(Gesture.Choice.ROCK);
        }
        else if (_gesture.ChosenHandSignal == Gesture.Choice.ROCK)
        {
           _gesture = new Gesture(Gesture.Choice.PAPER);           
        }
        else if (_gesture.ChosenHandSignal == Gesture.Choice.PAPER)
        {
           _gesture = new Gesture(Gesture.Choice.SCISSORS);                      
        }
        else if (_gesture.ChosenHandSignal == Gesture.Choice.SCISSORS)
        {
           _gesture = new Gesture(Gesture.Choice.ROCK);                      
        }
    }
}