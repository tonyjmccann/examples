public abstract class Player
{     
    protected string _name;
    
   public string Name
   {
       get => _name;
   }

   protected Gesture? _gesture;
    
   public Gesture? Gesture
   {
       get => _gesture;
   }

   public Player(string name)
   {
        _name = name;
   }
    
   public abstract Gesture PlayGesture(); 
}

