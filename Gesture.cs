public class Gesture
{
  public enum Choice  { ROCK, PAPER, SCISSORS}
  
  private Choice _chosenHandSignal;

  
   public Choice ChosenHandSignal
   {
       get => _chosenHandSignal;
   }
  
  public Gesture(Choice chosenHandSignal)
  {
     _chosenHandSignal = chosenHandSignal;
  }

  public bool Beats(Gesture otherGesture)
  {
     switch (_chosenHandSignal)
     { 
         case Choice.ROCK:
              return otherGesture.ChosenHandSignal == Choice.SCISSORS;
         case Choice.PAPER:
              return otherGesture.ChosenHandSignal == Choice.ROCK;
         case Choice.SCISSORS:
              return otherGesture.ChosenHandSignal == Choice.PAPER;
     }
     return false;
  }

  public override bool Equals(Object obj)
  {
     Gesture? gest = obj as Gesture;
     if (gest == null)
     {
            return false;
     }        
     else
     {
          switch (_chosenHandSignal)
          { 
               case Choice.ROCK:
                    return gest.ChosenHandSignal == Choice.ROCK;
               case Choice.PAPER:
                    return gest.ChosenHandSignal == Choice.PAPER;
               case Choice.SCISSORS:
                    return gest.ChosenHandSignal == Choice.SCISSORS;
          }
          
     }   
     return false;   
   }

   public override int GetHashCode()
   {
     return _chosenHandSignal.GetHashCode();
   }
}