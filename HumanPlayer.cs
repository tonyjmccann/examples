public class HumanPlayer : Player
{  
    public HumanPlayer(string name)
    : base(name)
    {

    }

    public override Gesture PlayGesture()
    {
        return _gesture;
    }

    public void ChooseNextGesture(Gesture gesture)
    {
         _gesture = gesture;
    }
}