﻿    class Program
    {
        private static void Main(string[] args)
        {   
              Console.WriteLine("-----------Welcome to Rock Paper Scissors game!--------------");
              Console.WriteLine("First player to win 3 rounds wins the game.");
               Console.WriteLine();
              IList<Player> players = ChoosePlayers();
              IList<GameRound> gameRounds = new List<GameRound>();            
              bool gameFinished = false;

              while (!gameFinished)
              {
                Console.WriteLine($"*******Rock paper scissors - Round {gameRounds.Count+1} start********)");
                GameRound gameRound = PlayRound(players[0], players[1]);   
                gameRounds.Add(gameRound);
            
                int player1Won = gameRounds.Where(c => c.RoundWinner == players[0]).Count();
                int player2Won = gameRounds.Where(c => c.RoundWinner == players[1]).Count();
                if (player1Won > 2)
                {
                    Console.WriteLine("Player one has won 3 rounds. Congratulations on winning the game. Press any key to exit.");
                    Console.ReadKey();
                    gameFinished = true;
                }
                if (player2Won > 2)
                {
                    Console.WriteLine("Player two has won 3 rounds. Congratulations on winning the game. Press any key to exit.");
                    Console.ReadKey();
                     gameFinished = true;
                }
              }             
        } 

        private static GameRound PlayRound(Player playerOne, Player playerTwo)
        {
            GameRound gameRound = new GameRound(playerOne, playerTwo);

            while(gameRound.RoundWinner == null)
            {
                             
                Gesture playerOneGesture = HumanPlayerGesturePrompt(playerOne.Name);
                ((HumanPlayer)playerOne).ChooseNextGesture(playerOneGesture); 

                if (playerTwo is HumanPlayer) 
                {   
                    Gesture playerTwoGesture = HumanPlayerGesturePrompt(playerTwo.Name);
                    ((HumanPlayer)playerTwo).ChooseNextGesture(playerTwoGesture); 

                    Console.WriteLine($"       {playerOne.Name} played {playerOneGesture.ChosenHandSignal}");
                    Console.WriteLine($"       {playerTwo.Name} played {playerTwoGesture.ChosenHandSignal}");
                }
                else if (playerTwo is ComputerPlayer) 
                {   
                    ((ComputerPlayer)playerTwo).ChooseNextGesture();

                    Console.WriteLine($"      {playerOne.Name} played {playerOneGesture.ChosenHandSignal}");
                    Console.WriteLine($"      {playerTwo.Name} played {playerTwo.Gesture.ChosenHandSignal}");                    
                }                 
                
                gameRound.PlayRound();                

                if (gameRound.RoundWinner == playerOne)
                {
                    Console.WriteLine($"{playerOne.Name} won this round.");
                    Console.WriteLine();
                    return gameRound;
                } 
                else if (gameRound.RoundWinner == playerTwo)
                {
                    Console.WriteLine($"{playerTwo.Name} won this round.");
                    Console.WriteLine();
                    return gameRound;
                }
                else if (gameRound.RoundWinner == null)
                {
                    Console.WriteLine("No clear winner. The round restarts.");
                    Console.WriteLine();
                }           
            }
            return null;                      
        }

        private static IList<Player> ChoosePlayers()
        {
            IList<Player> playerList = new List<Player>();            

            bool choicePlayerTypeMade = false;
            while (choicePlayerTypeMade == false) 
            {
                Console.WriteLine("Enter 1 for a human versus human game.");
                Console.WriteLine("Enter 2 for a human versus computer game.");
                ConsoleKeyInfo keyPress = Console.ReadKey(); 
                Console.WriteLine();
                if (keyPress.KeyChar == '1')
                {  
                    Player playerOne = CreateHumanPlayer("<Player 1 - Human>");
                    Player playerTwo = CreateHumanPlayer("<Player 2 - Human>");
                    playerList.Add(playerOne);
                    playerList.Add(playerTwo);
                     return playerList;
                }
                else if (keyPress.KeyChar == '2')
                {
                    Player playerOne = CreateHumanPlayer("<Player 1 - Human>");
                    Player playerTwo = CreateComputerPlayer("<Player 2 - Computer>");
                    playerList.Add(playerOne);
                    playerList.Add(playerTwo);  
                     return playerList;              
                }
                else
                {
                    Console.WriteLine("sorry invalid key pressed.");
                }
               
            }  
             return playerList;        
             
        }
        
        private static Player CreateHumanPlayer(string playerName)
        {
           return new HumanPlayer(playerName);   
        }

        private static Player CreateComputerPlayer(string playerName)
        {
            return new ComputerPlayer(playerName);
        }

        private static Gesture HumanPlayerGesturePrompt(string playerName)
        {
            bool choicePlayerGestureMade = false;
            while (choicePlayerGestureMade == false) 
            {
                Console.WriteLine($"{playerName} please press key for gesture chosen? (R - rock, P - paper, S - Scissors.)"); 
                 Console.WriteLine($"The key you press will not be shown on screen.");                
                ConsoleKeyInfo keyPress = Console.ReadKey(true); 
                if (keyPress.KeyChar == 'R' || keyPress.KeyChar == 'r')
                {  
                    return new Gesture(Gesture.Choice.ROCK);                    
                }
                else if (keyPress.KeyChar == 'P' || keyPress.KeyChar == 'p')
                {
                    return new Gesture(Gesture.Choice.PAPER);  
                }
                else if (keyPress.KeyChar == 'S'|| keyPress.KeyChar == 's')
                {
                    return new Gesture(Gesture.Choice.SCISSORS);  
                }
                else
                {
                     Console.WriteLine($"{playerName} Sorry key choice is invalid. (R - rock, P - paper, S - Scissors)");        
                }
            }
            return null;         
        }       
    }

